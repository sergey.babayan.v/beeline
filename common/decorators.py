from functools import wraps


def aslist(iterable):
    """Function decorator to transform a generator into a list"""

    @wraps(iterable)
    def wrapper(*args, **kwargs):
        return list(iterable(*args, **kwargs))

    return wrapper
