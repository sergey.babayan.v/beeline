from enum import Enum
from typing import Any


class BaseEnum(Enum):
    @classmethod
    def list(cls) -> tuple[tuple[Any, Any]]:
        return tuple((field.name, field.value) for field in cls)
