from typing import Dict

from rest_framework.serializers import Serializer


def inline_serializer(name: str, fields: Dict[str, object]) -> type:
    return type(name, (Serializer,), fields)
