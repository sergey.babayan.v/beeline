from enum import Enum

from rest_framework.fields import Field


class EnumOut(Field):
    """
    Custom output for enum types
    """
    def to_representation(self, value: Enum):
        return {"name": value.name, "value": value.value}

    def to_internal_value(self, **kwargs):
        pass
