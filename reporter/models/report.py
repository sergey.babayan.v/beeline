from django.db import models

from .. import managers
from .. import querysets


class Report(models.Model):
    file = models.FileField(upload_to="reports")
    created_at = models.DateTimeField(auto_now=True)
    query_data = models.TextField()

    objects: managers.Report = managers.Report()
    repo: querysets.Report = querysets.Report()
