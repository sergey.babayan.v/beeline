from datetime import datetime
from typing import Iterable

from django.db.models import QuerySet

from corporation import models as corp_models


class Report(QuerySet):
    def meets_from_to(
        self, *, from_: datetime, to: datetime
    ) -> Iterable["corp_models.Meet"]:
        return corp_models.Meet.objects.filter(
            datetime__lte=to, datetime__gte=from_
        ).select_related()
