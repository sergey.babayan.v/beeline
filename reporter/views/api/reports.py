import io
from datetime import datetime
from typing import Union

from django.core.files.base import ContentFile
from django.http import HttpResponse, FileResponse
from drf_spectacular.utils import extend_schema, OpenApiParameter
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from common.serializers import inline_serializer

from ... import models


class MakeReport(APIView):
    query_serializer = inline_serializer(
        "MeetsReport",
        {"from": serializers.DateTimeField(), "to": serializers.DateTimeField()},
    )

    @extend_schema(
        responses={200: {}},
        operation_id="report",
        parameters=[OpenApiParameter(name="from"), OpenApiParameter(name="to")],
    )
    def get(self, request: Request) -> Union[FileResponse, Response]:
        in_: serializers.Serializer = self.query_serializer(data=request.query_params)
        if in_.is_valid():
            validated_data = in_.validated_data
            from_: datetime = validated_data["from"]
            to: datetime = validated_data["to"]
            if from_ >= to:
                raise ValidationError(detail="Укажите валидный промежуток")
            csv: io.BytesIO = models.Report.objects.meets_from_to_csv(
                from_=from_, to=to
            )
            return FileResponse(csv, as_attachment=True, filename="data.csv")
        return Response(in_.errors)
