import csv
import io
from datetime import datetime
from itertools import chain
from typing import Iterable

from django.db.models import Manager

from corporation import models as corp_models
from .. import models


class Report(Manager):
    def __get_rows_for_meet_csv(
        self, *, meets: Iterable["corp_models.Meet"]
    ) -> Iterable[tuple]:
        for meet in meets:
            client: corp_models.Client = meet.client
            employee: corp_models.Employee = meet.employee
            yield [
                f"{employee.first_name} {employee.last_name} {employee.middle_name}",
                f"{client.first_name} {client.last_name} {client.middle_name}",
                client.age,
                f"{meet.datetime:%Y-%m-%d %H:%M}",
            ]

    def __write_csv(self, *, meets: Iterable["corp_models.Meet"]) -> io.BytesIO:
        s = io.StringIO()
        writer = csv.writer(s)
        writer.writerows(
            chain(
                [
                    (
                        "ФИО Сотрудника",
                        "ФИО Клиента",
                        "Возраст клиента",
                        "Дата и время встречи",
                    )
                ],
                self.__get_rows_for_meet_csv(meets=meets),
            )
        )
        s.seek(0)
        buf = io.BytesIO()
        buf.write(s.getvalue().encode())
        buf.seek(0)
        return buf

    def meets_from_to_csv(self, *, from_: datetime, to: datetime) -> io.BytesIO:
        meets: Iterable[corp_models.Meet] = models.Report.repo.meets_from_to(
            from_=from_, to=to
        )
        buf: io.BytesIO = self.__write_csv(meets=meets)
        return buf
