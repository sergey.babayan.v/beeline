from django.urls import path

from ..views import api


app_name = "reporter"
urlpatterns = [path("meets/", api.MakeReport.as_view())]
