FROM python:3.9
ENV PYTHONUNBUFFERED=1
ENV PATH="${PATH}:/root/.local/bin"
RUN  pip install --upgrade pip
RUN apt-get -q update && apt-get -qy install netcat && pip install poetry
RUN poetry config virtualenvs.create false --local
WORKDIR /app
COPY poetry.lock pyproject.toml /app/
RUN poetry install
COPY entrypoint.sh wait-for /app/
RUN chmod a+x ./entrypoint.sh ./wait-for
COPY . /app/