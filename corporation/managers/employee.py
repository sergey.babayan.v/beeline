from django.db.models import Manager

from .. import models


class Employee(Manager):
    def meet_client(
        self, *, employee: "models.Employee", client: "models.Client"
    ) -> "models.Meet":
        return models.Meet.objects.create(client=client, employee=employee)
