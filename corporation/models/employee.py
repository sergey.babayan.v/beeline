from django.db import models

from .. import querysets
from .. import managers


class Meet(models.Model):
    employee = models.ForeignKey("Employee", on_delete=models.CASCADE)
    client = models.ForeignKey("corporation.Client", on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)


class Employee(models.Model):
    first_name = models.CharField(verbose_name="Имя", max_length=255)
    middle_name = models.CharField(verbose_name="Отчество", max_length=255)
    last_name = models.CharField(verbose_name="Фамилия", max_length=255)
    position = models.CharField(verbose_name="Должность", max_length=255)

    meets = models.ManyToManyField(
        "corporation.Client", through=Meet, related_name="meets"
    )

    objects: managers.Employee = managers.Employee()
    repo: querysets.Employee = querysets.Employee.as_manager()

    class Meta:
        db_table = "employee"
        verbose_name = "Сотрудник"
        verbose_name_plural = "Сотрудники"
