from django.db import models

from .. import managers
from .. import querysets


class Client(models.Model):
    first_name = models.CharField(verbose_name="Имя", max_length=255)
    middle_name = models.CharField(verbose_name="Отчество", max_length=255)
    last_name = models.CharField(verbose_name="Фамилия", max_length=255)
    age = models.IntegerField(verbose_name="Возраст")
    address = models.TextField()

    objects: managers.Client = managers.Client()
    repo: querysets.Client = querysets.Client.as_manager()

    class Meta:
        db_table = "client"
        verbose_name = "Клиет"
        verbose_name_plural = "Клиенты"
