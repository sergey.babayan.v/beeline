from rest_framework import serializers

from .... import models


class EmployModel(serializers.ModelSerializer):
    class Meta:
        model = models.Employee
        fields = ("first_name", "last_name", "middle_name", "position", "id")
        readonly = ("id",)


class ClientModel(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = ("first_name", "last_name", "middle_name", "age", "address", "id")
        readonly = ("id",)


class MeetModel(serializers.ModelSerializer):
    client = ClientModel()
    employee = EmployModel()

    class Meta:
        model = models.Meet
        fields = ("client", "employee", "id")
        readonly = ("id",)
