from typing import Optional

from drf_spectacular.utils import extend_schema
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import serializers

from common.serializers import inline_serializer
from ... import models
from . import serializer


class EmployeeViewSet(viewsets.ModelViewSet):
    model = models.Employee
    queryset = models.Employee.repo.all()
    serializer_class = serializer.EmployModel

    meet_serializer = inline_serializer(
        "MeetIn",
        {"client": serializers.IntegerField(), "employee": serializers.IntegerField()},
    )

    def check_existing_of_instances(
        self, *, employee_id: int, client_id: int
    ) -> tuple[models.Employee, models.Client]:
        employee: Optional[models.Employee] = models.Employee.repo.by_id(
            id_=employee_id
        )
        if employee is None:
            raise ValidationError(detail="Не найден такой сотрудник")
        client: Optional[models.Client] = models.Client.repo.by_id(id_=client_id)
        if client is None:
            raise ValidationError(detail="Не найден такой клиент")
        return employee, client

    @extend_schema(responses={200: serializer.MeetModel}, request=meet_serializer)
    @action(methods=["post"], name="meet", url_name="meet", detail=False)
    def meet_client(self, request: Request) -> Response:
        in_: serializers.Serializer = self.meet_serializer(data=request.data)
        if in_.is_valid():
            employee, client = self.check_existing_of_instances(
                employee_id=in_.validated_data["employee"],
                client_id=in_.validated_data["client"],
            )
            meet: models.Meet = models.Employee.objects.meet_client(
                employee=employee, client=client
            )
            return Response(serializer.MeetModel(instance=meet).data)
        return Response(in_.errors, status=400)


class ClientViewSet(viewsets.ModelViewSet):
    model = models.Client
    queryset = models.Client.repo.all()
    serializer_class = serializer.ClientModel
