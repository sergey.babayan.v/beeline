from rest_framework import routers

from ..views import api

app_name = "corporation"
router = routers.SimpleRouter()
router.register("employee", api.EmployeeViewSet, basename="employee")
router.register("client", api.ClientViewSet, basename="client")


url_patterns = []
