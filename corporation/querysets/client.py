from typing import Optional

from django.db.models import QuerySet

from .. import models


class Client(QuerySet):
    def by_id(self, *, id_: int) -> Optional["models.Client"]:
        return self.filter(pk=id_).first()
