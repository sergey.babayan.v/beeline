from typing import Optional

from django.db.models import QuerySet
from .. import models


class Employee(QuerySet):
    def by_id(self, *, id_: int) -> Optional["models.Employee"]:
        return self.filter(pk=id_).first()
