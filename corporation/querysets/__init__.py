from .employee import Employee
from .client import Client

__all__ = (Employee, Client)
