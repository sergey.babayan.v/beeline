from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
)

from corporation.urls.api import router as corp_router

routers = corp_router.urls

api_urls = [path("", include("reporter.urls.api"))]

api_urlpatterns = [
    path("v1/", include(routers + api_urls)),
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path("docs/", SpectacularRedocView.as_view(url_name="schema"), name="redoc"),
]

urlpatterns = [path("admin/", admin.site.urls), path("api/", include(api_urlpatterns))]
